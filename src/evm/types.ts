import { BigNumberish } from 'ethers';
import { IERC20, USDCDeposit } from '../types/abi';

export enum ChainId {
    PolygonTestnet = 80002,
    LocalHardhat = 31337,
}

export interface ContractAddresses {
    Deposit: string;
    USDC: string;
}

export interface TokenDecimals {
    USDC: number;
}

export interface IERC20ParseUnits {
    parseUnits(value: string): bigint;
    formatUnits(value: BigNumberish): string;
    formatUnitsWithDecimalPlaces(value: BigNumberish, decimalPlaces: number): string;
    getDecimals(): number;
}

export interface Contracts {
    depositContract: USDCDeposit;
    usdcContract: IERC20 & IERC20ParseUnits;
}
