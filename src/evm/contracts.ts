import { Contract, ContractRunner, BigNumberish, formatUnits, parseUnits } from 'ethers';
import { getContractAddressesForChainOrThrow, getTokenDecimalsForChainOrThrow } from './addresses';
import { IERC20ParseUnits, Contracts } from './types';
import { USDCDeposit, IERC20 } from '../types/abi';

import { default as IERC20_ABI } from '../../abi/IERC20.json';
import { default as DEPOSIT_ABI } from '../../abi/USDCDeposit.json';

/**
 * Get contract instances that target the Aurora mainnet
 * or a supported testnet. Throws if there are no known contracts
 * deployed on the corresponding chain.
 * @param chainId The desired chain id
 * @param signerOrProvider The ethers v5 signer or provider
 */
export function getContractsForChainOrThrow(chainId: number, signerOrProvider: ContractRunner) {
    const addresses = getContractAddressesForChainOrThrow(chainId);
    const decimals = getTokenDecimalsForChainOrThrow(chainId).USDC;

    const depositContract = new Contract(
        addresses.Deposit,
        DEPOSIT_ABI,
        signerOrProvider
    ) as unknown as USDCDeposit;

    const parseUnitsMixin: IERC20ParseUnits = {
        parseUnits: (value: string) => {
            return parseUnits(value, decimals);
        },
        formatUnits: (value: BigNumberish) => {
            return formatUnits(value, decimals);
        },
        formatUnitsWithDecimalPlaces: (value: BigNumberish, decimalPlaces: number) => {
            const valueBN = BigInt(value);
            const exponent = BigInt(decimals - decimalPlaces);
            const remainder = exponent > 0 ? (valueBN % (10n ** exponent)) : 0n;
            return formatUnits(valueBN - remainder, decimals);
        },
        getDecimals: () => {
            return decimals;
        }
    }

    const usdcContract = new Contract(addresses.USDC, IERC20_ABI, signerOrProvider) as unknown as IERC20;
    const usdcContractExtended = Object.assign(usdcContract, parseUnitsMixin);

    return {
        depositContract,
        usdcContract: usdcContractExtended
    } as Contracts;
}