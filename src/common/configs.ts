export enum Numbers {
    ReleasePeriodInSeconds = 0,
    AcceptPeriodInSeconds,
    ClaimAfterInSeconds,
    AprovePeriodInSeconds,
    FeeNumerator,
    FeeDenumerator,
    MinFeeInWei,
    InviterFeePercentage
}

export enum Scales {
    AcceptOrderTime = 0,
    CompleteOrderTime,
    ApproveResultsScore,
}

export enum Addresses {
    Deposit = 0,
    USDC,
}

export enum Environment {
    Mainnet = 0,
    Testnet
}

export type ConfigNumbers = {
    [key in Numbers]: number | bigint;
}

export type PartialConfigNumbers = Omit<ConfigNumbers, Numbers.MinFeeInWei>;

export type ConfigScales = {
    [key in Scales]: { bound: number; value: number }[];
}

export type ConfigInterface = {
    numbers: ConfigNumbers,
    scales: ConfigScales,
};