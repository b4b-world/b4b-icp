export { ReturnError, CreateOrderInput } from '../../declarations/backend/backend.did';
import { HttpAgent, Identity } from '@dfinity/agent';
import { createActor, CreateActorOptions } from '../../declarations/backend';

export type CoreActor = ReturnType<typeof createActor>;

export function getCoreActor(canisterId: string, host?: string, identity?: Identity): CoreActor {
    const options: CreateActorOptions | undefined = host ? {
        agent: new HttpAgent({ host, identity })
    } : undefined;
    const actor = createActor(canisterId, options);

    return actor;
}