export interface IListenerOptions {
    pollInterval: number;
    backoff: number;
    chunkSize: number;
}

export const OPTIONS: IListenerOptions = {
    pollInterval: 10000,
    backoff: 10000,
    chunkSize: 1000,
};