import { LogParser, Patch } from './parser';
import { Timer, ClockTimer } from './timer';

import { HttpAgent } from '@dfinity/agent';

import { createActor, CreateActorOptions } from '../../declarations/backend';
import { LogViewResponse, LogView } from '../../declarations/backend/backend.did';
import { BACKEND_CANISTER_ID } from '../config';
import { OPTIONS } from './options';

import { EventEmitter } from 'node:events';

export interface PatchSeries {
    chainId: number;
    timestamp: number;
    patches: Patch[];
}

export type BlockCallback = (patchSeries: PatchSeries) => void | Promise<void>;

export class EventListener {
    private parser: LogParser;
    private actor: ReturnType<typeof createActor>;
    private running = false;
    private emitter: EventEmitter;
    private timer: Timer;

    constructor(canisterId: string, host?: string, timer?: Timer) {
        const options: CreateActorOptions | undefined = host ? {
            agent: new HttpAgent({ host })
        } : undefined;
        this.actor = createActor(canisterId, options);
        this.parser = new LogParser();
        this.emitter = new EventEmitter();
        this.timer = timer ?? new ClockTimer();
        console.log("Listener: initialized");
    }

    async _poll(from?: number) {
        console.log("Listener: polling_0 ", from);

        // TODO: handle repetetive errors
        while (this.running) {
            try {
                let len = Number(await this.actor.get_lates_event_idx());
                from = from ?? len;

                let steps = Math.ceil((len - from) / OPTIONS.chunkSize);

                console.log("Listener:", from, len, steps);

                for (let i = 0; i < steps; i++) {
                    const toBlock: number = Math.min(from + OPTIONS.chunkSize, len);
                    console.log("Listener: getEvents", from, toBlock);
                    const patches = await this._getEvents(from, toBlock);
                    if (patches.length != 0) {
                        this.emitter.emit('block', {
                            chainId: 0,
                            timestamp: 0,
                            patches,
                        });
                    }
                    console.log("Listener: patches", patches.length);
                    from = toBlock;
                }

                await this.timer.timeout(OPTIONS.pollInterval);
            } catch (err) {
                console.error('Error', err);
                await this.timer.timeout(OPTIONS.backoff);
            }
        }
    }

    private async _getEvents(from: number, to?: number): Promise<Patch[]> {
        const logs: LogViewResponse = await this.actor.view_events_log({
            to: to ? [BigInt(to)] : [],
            from: [BigInt(from)]
        });

        const patches = logs.logs.map((log) => {
            return this.parser.parse(log);
        });

        return patches;
    }

    public stop() {
        this.running = false;
    }

    async start(startBlock?: number) {
        this.running = true;

        try {
            this._poll(startBlock);
        } catch (err) {
            console.log(err);
        }
    }

    public on(callback: BlockCallback) {
        this.emitter.on('block', callback);
    }

    public once(callback: BlockCallback) {
        this.emitter.once('block', callback);
    }
}
