import { Campaign, CampaignStatus, OrderType } from '../common/types';

import { Event, OrderState, OrderStatus } from '../../declarations/backend/backend.did';

type OrderCreatedEvent = Extract<Event, { OrderCreated: unknown }>;
type OrderUpdatedEvent = Extract<Event, { OrderUpdated: unknown }>;
type InfluencerProfileCreatedEvent = Extract<Event, { InfluencerProfileCreated: unknown }>;

const DECIMALS = Number(10 ** 6);

export enum PatchType {
    B4B,
    UniqueIdentity,
}

export interface CampaignInfo extends Partial<Campaign> {
    orderId: string;
    royalty?: number;
}

export interface TransferInfo {
    from: string;
    to: string;
    tokenId: string;
}

export interface Patch {
    type: PatchType;
    data: CampaignInfo | TransferInfo;
}

export class LogParser {
    constructor() {}

    public parse(log: Event): Patch {
        let patch: Patch;
        let topics = Object.keys(log);
        switch (topics[0]) {
            // OrderCreated
            case 'OrderCreated':
                {
                    patch = this._parseOrderCreated((log as OrderCreatedEvent).OrderCreated);
                }
                break;

            // OrderUpdated
            case 'OrderUpdated':
                {
                    patch = this._parseOrderUpdated((log as OrderUpdatedEvent).OrderUpdated);
                }
                break;

            case 'InfluencerProfileCreated':
                {
                    patch = this._parseInfluencerCreated((log as InfluencerProfileCreatedEvent).InfluencerProfileCreated)
                }
                break;

            default:
                throw Error();
        }

        return patch;
    }

    private _parseOrderState(state: OrderState): CampaignStatus {
        switch (Object.keys(state)[0]) {
            case 'OrderCreated':
                return CampaignStatus.OrderCreated;
            case 'OrderAccepted':
                return CampaignStatus.OrderAccepted;
            case 'ResultApproved':
                return CampaignStatus.ResultAproved;
            case 'OrderFilled':
                return CampaignStatus.OrderFilled;
            case 'OrderRejected':
                return CampaignStatus.OrderRejected;
            default:
                return CampaignStatus.NotExist;
        }
    }

    private _parseOrderCreated(event: OrderCreatedEvent[keyof OrderCreatedEvent]): Patch {
        const patch: Patch = {
            type: PatchType.B4B,
            data: {
                orderId: '0x' + event.order_id.toString(16),
                brandAddr: event.order_status.advertiser_principal.toString(),
                influencerId: event.order_status.influencer_id.toString(),
                releaseDate: Number(event.order_status.release_timestamp),
                orderCreationDate: Number(event.order_status.creation_timestamp),
                orderType: OrderType.Post,
                price: Number(event.order_status.amount) / DECIMALS,
                fee: 0,
                status: this._parseOrderState(event.order_status.state),
                data: event.order_status.data_hash,
            },
        };

        console.info(`[OrderCreated]: order ${(patch.data as CampaignInfo).orderId}`);

        return patch;
    }

    private _parseOrderUpdated(event: OrderUpdatedEvent[keyof OrderUpdatedEvent]): Patch {
        const status = this._parseOrderState(event.order_state);
        const patch: Patch = {
            type: PatchType.B4B,
            data: {
                orderId: '0x' + event.order_id.toString(16),
                rating: 0,
                status,
            },
        };

        // if (status == CampaignStatus.OrderFilled) {
        //     patch.data = {
        //         ...patch.data,
        //         orderCompletionDate: Date.now()
        //     };
        // }

        switch (status) {
            case CampaignStatus.OrderFilled:
                patch.data = {
                    ...patch.data,
                    orderCompletionDate: Math.floor(Date.now() / 1000) // TODO: take from canister
                };
                break;
            case CampaignStatus.ResultAproved:
            case CampaignStatus.ResultAutoAproved:
            case CampaignStatus.ResultAprovedAdmin:
                patch.data = {
                    ...patch.data,
                    royalty: 0,
                };
                break;
        }

        console.info(
            `[OrderUpdated]: order ${(patch.data as CampaignInfo).orderId} with status ${CampaignStatus[status]}`,
        );

        return patch;
    }

    private _parseInfluencerCreated(event: InfluencerProfileCreatedEvent[keyof InfluencerProfileCreatedEvent]): Patch {
        const patch: Patch = {
            type: PatchType.UniqueIdentity,
            data: {
                from: "0x0",
                to: event.principal.toString(),
                tokenId: event.influencer_id.toString(),
            } as TransferInfo,
        };

        return patch;
    }
}
