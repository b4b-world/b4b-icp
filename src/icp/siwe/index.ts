export * from "./helpers";

import { createActor, CreateActorOptions } from "../../declarations/ic_siwe_provider";
import { HttpAgent, Cbor, Certificate } from "@dfinity/agent";
import { Principal } from "@dfinity/principal";
import {
    Delegation,
    DelegationChain,
    DelegationIdentity,
    Ed25519KeyIdentity,
    type SignedDelegation,
} from "@dfinity/identity";
import { asDerEncodedPublicKey, asSignature } from "./utils";
import { LoginDetails } from "../../declarations/ic_siwe_provider/ic_siwe_provider.did";
import { getAddress } from 'ethers';

export class SiweProvider {
    private readonly actor: ReturnType<typeof createActor>;

    constructor(canisterId: string, host?: string) {
        const options: CreateActorOptions | undefined = host ? {
            agent: new HttpAgent({ host })
        } : undefined;

        this.actor = createActor(canisterId, options);
    }

    public async prepareLoginOrThrow(address: string) {
        const response = await this.actor.siwe_prepare_login(address);
        if ('Err' in response) {
            throw new Error(response.Err);
        }

        return response.Ok;
    }

    public async loginOrThrow(address: string, signature: string): Promise<[LoginDetails, Ed25519KeyIdentity]> {
        const sessionIdentity = Ed25519KeyIdentity.generate();
        const sessionPubKey = sessionIdentity.getPublicKey().toDer();

        const loginResponse = await this.actor.siwe_login(signature, address, new Uint8Array(sessionPubKey));

        if ('Err' in loginResponse) {
            throw new Error(loginResponse.Err);
        }

        return [loginResponse.Ok, sessionIdentity];
    }

    public async getDelegationIdentityOrThrow(
        address: string, 
        sessionIdentity: Ed25519KeyIdentity, 
        loginResponse: LoginDetails
    ): Promise<[DelegationIdentity, DelegationChain]> 
    {
        const sessionPubKey = sessionIdentity.getPublicKey().toDer();

        const delegationResponse = await this.actor.siwe_get_delegation(
            address,
            new Uint8Array(sessionPubKey),
            loginResponse.expiration
        );
        if ("Err" in delegationResponse) {
            throw new Error(delegationResponse.Err);
        }

        const signedDelegation = delegationResponse.Ok;

        // Create a new delegation chain from the delegation.
        const delegations: SignedDelegation[] = [
            {
                delegation: new Delegation(
                    (signedDelegation.delegation.pubkey as Uint8Array).buffer,
                    signedDelegation.delegation.expiration,
                    signedDelegation.delegation.targets[0] as Principal[]
                ),
                signature: asSignature(signedDelegation.signature),
            },
        ];
        const delegationChain = DelegationChain.fromDelegations(
            delegations,
            asDerEncodedPublicKey(loginResponse.user_canister_pubkey)
        );

        // Create a new delegation identity from the session identity and the
        // delegation chain.
        const identity = DelegationIdentity.fromDelegation(
            sessionIdentity,
            delegationChain
        );

        return [identity, delegationChain];
    }

    public async getPrincipal(evmAddress: string): Promise<Principal> {
        const address = getAddress(evmAddress);
        const principal = await this.actor.get_principal(address);
        if ('Err' in principal) throw Error(principal.Err);
        return Principal.from(principal.Ok);
    }

    public async getEvmAddress(principal: Principal): Promise<string> {
        const address = await this.actor.get_address(principal.toUint8Array() as any);
        if ('Err' in address) throw Error(address.Err);
        return address.Ok;
    }
}