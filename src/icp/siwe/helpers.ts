import { requestIdOf, Cbor } from "@dfinity/agent";
import { verifyCanisterSig } from './certificates';
import { encodeBase64, decodeBase64 } from 'ethers';
import { Principal } from '@dfinity/principal';

const DOMAIN_SEPARATOR = new TextEncoder().encode('\x1Aic-request-auth-delegation');

export interface VerificationRequest {
    signatureBytes: Uint8Array,
    publicKey: Uint8Array
    challenge: Uint8Array
}

export function encodeVerificationRequest(request: VerificationRequest): string {
    const obj = {
        signatureBytes: encodeBase64(request.signatureBytes),
        publicKey: encodeBase64(request.publicKey),
        challenge: encodeBase64(request.challenge)
    }

    return JSON.stringify(obj);
}

export function decodeVerificationRequest(requestStr: string): VerificationRequest {
    const obj = JSON.parse(requestStr);

    return {
        signatureBytes: decodeBase64(obj.signatureBytes),
        publicKey: decodeBase64(obj.publicKey),
        challenge: decodeBase64(obj.challenge)
    };
}

export async function recoverDelegationOrThrow(encodedRequest: string, rootKey: Uint8Array) {
    const request = decodeVerificationRequest(encodedRequest);
    const challengeBytes = new Uint8Array([
        ...DOMAIN_SEPARATOR,
        ...request.challenge
    ]);
    const result = await verifyCanisterSig(
        challengeBytes,
        request.signatureBytes,
        request.publicKey,
        rootKey
    );

    if (!result) throw Error("Verification failed!");

    return Principal.selfAuthenticating(request.publicKey);
}