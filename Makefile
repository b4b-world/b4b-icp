build_test:
	RUSTFLAGS='--cfg mock' cargo build --target wasm32-unknown-unknown --release; \

test:
	@{ \
		$(MAKE) build_test; \
		export POCKET_IC_BIN=../../pocket-ic; \
		export BACKEND_PATH=../../target/wasm32-unknown-unknown/release/backend.wasm; \
		cargo test --test integration_tests -- --nocapture; \
	}

clean:
	cargo clean

candid_extractor:
	candid-extractor target/wasm32-unknown-unknown/release/backend.wasm > candid/backend.did

build_localnode_test:
	dfx canister create backend; \
	dfx canister create ic_siwe_provider; \
	dfx canister create evm_rpc; \
	dfx build backend; \

deploy_local:
	dfx deploy backend; \
	dfx deploy evm_rpc --argument '(record { nodesInSubnet = 28 })'; \
	./scripts/sh/set_config.sh; \
	./scripts/sh/set_chain_config.sh; \
	./scripts/sh/deploy_siwe_provider_local.sh; \

build_ts_canister_wrappers:
	dfx generate ic_siwe_provider; \
	dfx generate backend; \
	npx import2require src/declarations/**/*.js; \
	./scripts/sh/loop_js.sh ./src/declarations; \

build_ts_contract_wrappers:
	npx hardhat compile; \
	npm run build:abi; \
	rm -r src/types/artifacts; \
	rm -r src/types/factories; \
	rm src/types/index.ts; \
	rm src/types/hardhat.d.ts; \