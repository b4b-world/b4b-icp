import { HardhatUserConfig } from "hardhat/config";
require("@nomicfoundation/hardhat-toolbox");
require('@typechain/hardhat')
require('@nomicfoundation/hardhat-ethers')
// require('@nomicfoundation/hardhat-chai-matchers')
require('hardhat-abi-exporter');
// require('hardhat-packager');


require('dotenv').config();

const ETH_PRIVATE_KEY = process.env.ETH_PRIVATE_KEY;

/** @type import('hardhat/config').HardhatUserConfig */
const config: HardhatUserConfig = {
  solidity: "0.8.18",
  networks: {
    testnet_polygon: {
        url: "https://rpc-amoy.polygon.technology/",
        accounts: [`0x${ETH_PRIVATE_KEY}`],
        chainId: 80002,
        // gasPrice: 30 * 1000000000
    }
  },
  typechain: {
    outDir: 'src/types',
    target: 'ethers-v6',
    alwaysGenerateOverloads: false, // should overloads with full signatures like deposit(uint256) be generated always, even if there are no overloads?
    externalArtifacts: ['abi/**/*.json'], // optional array of glob patterns with external artifacts to process (for example external libs from node_modules)
    dontOverrideCompile: false // defaults to false
  },
  abiExporter: {
    path: './abi',
    runOnCompile: true,
    flat: true,
    clear: true,
    only: ["USDCDeposit", "IERC20"],
    format: "json"
  },
  packager: {
    contracts: [
      'USDCDeposit'
    ],
    includeFactories: false,
  },
};

export default config;