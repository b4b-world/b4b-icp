![B4B](/media/header.jpeg)

[B4B](https://b4b.app) is a Web3 influencer marketing protocol that connects advertisers with influencers who are willing to publish ads on their channels. The B4B protocol ensures the protection of advertisers and publishers by escrowing the budget and maintaining a transparent history of ad orders owned by users. Currently, it supports content publishing on three social media platforms: Twitter, Telegram, and Lens.

B4B is already live on 6 EVM chains. Currently, ad orders are created separately on each network. Data from Smart Contracts deployed on multiple networks are aggregated and indexed on the off-chain centralized backend, but cannot be transparently used for on-chain mechanics. The multi-chain deployment limits our ability to integrate rating systems, on-chain tokenomics, and user incentivization mechanics.

`b4b-icp` is an attempt to develop a base layer for B4B that bridges payments from different chains and aggregates data on the [Internet Computer](https://internetcomputer.org). The project's objective is to improve the experience for the supply-side of the marketplace (*influencers*) without affecting the payment process of the demand-side (*advertisers*). And the main goal is to gather data from all sources into a single chain for future use in reputation management, scoring, and predictions.

## Introduction

There are two main sides of B4B marketplace - influencers and advertisers. The diagram below give a high-level overview of how each side interacts with B4B.

**Advertiser's flow:**

- 1) Pay and lock advertising budget in USDC within B4B's smart contract on any supported EVM network.

- 2) Get the hash of the finalized transaction with funds locking.

- 3) Call ICP's canister with the transaction hash as an argument to create ad order

- 7) Review the posted content and call the canister to approve the result if the post meets your requirements, or reject it otherwise.

**Influencer's flow:**

- 4) Check the validity of the payment transaction and call the canister. If it's valid, accept the order.

- 5) The canister will request and verify transaction data via [evm-rpc](https://github.com/internet-computer-protocol/evm-rpc-canister) to prevent fraud from influencer side

- 6) - Publish the content on the social media and add the link on the post

      - Submit the link on the posted content and complete order

- 8) If an influencer publishes a post that meets the advertiser's requirements, the canister can generate a t-ECDSA certificate. This certificate can unlock funds on the EVM chain and transfer them to the influencer's account.

- 9) - Pass the generated t-ECDSA certificate to a Solidity contract on the EVM chain to unlock funds.

     - In case the influencer fails, the advertiser can request a t-ECDSA certificate from the canister and return the locked budget.


![B4B - Flow](/media/flow.png)


## Features

- **Payments in USDC on EVM Chains:** Advertisers can make payments in stable tokens on their preferred network.

- **Seamless UX for Users with EVM Wallets:** Users only need EVM wallets to use the platform. They can sign into IC using any EVM wallet they prefer via [ic-siwe](https://github.com/kristoferlund/ic-siwe/tree/main). Canister balance covers all gas on IC, eliminating the need for users to pay gas when using the protocol on IC.

- **Withdrawals on EVM:** Fund withdrawals on EVM are executed using t-ECDSA request signing, initiated from the canister.

- **Escrowing payments**: Funds are locked until the ad is published on social media or one of the parties decides not to participate.

- **Collecting ad orders data:**  Basis for future extensions to gather data on ad performance.

## 👀 Try the live demo:

Influencer demo app:  https://icp-influencer.b4b.app/

Advertiser demo app: https://icp.b4b.app/


## Instalation

Deploy contract on EVM network:

```bash
npx hardhat compile
npx hardhat --network network run scripts/deploy_deposit.js
```


Deposit funds on EVM chain:

```bash
npx hardhat --network network run scripts/call_deposit.js
```

Get lock-transaction receipt on ICP by hash:

```bash
dfx canister call backend --wallet $(dfx identity get-wallet) --with-cycles 600000000 eth_transaction_receipt "0x_tx_hash"
```


Get t-ECDSA signed data for unlocking funds on EVM chain:

```bash
dfx canister call backend --wallet $(dfx identity get-wallet) --with-cycles 600000000 withdraw "0x_address_from"
```

## Roadmap
The project is still in active development. 

- [x]  USDC payments on EVM chains
- [x]  EVM transaction retrieval from IC via [evm-rpc](https://github.com/internet-computer-protocol/evm-rpc-canister)
- [x]  Fund unlocking on EVM with t-ECDSA signed requests
- [x]  Ethereum wallet sign-in via [ic-siwe](https://github.com/kristoferlund/ic-siwe/tree/main)
- [x]  Ad Order management logic on IC
- [x]  Frontend integration with IC canisters
- [x]  Backend integration with IC canisters
- [ ]  Improved access control and time-limited logic for order management
- [ ]  Security Audits
- [ ]  Constructing a reputation layer for influencers and advertisers on IC
- [ ]  More reputation data from various sources such as social media, wallets, and marketing experts
- [ ]  Scores and predictions based on the collected data 
- [ ]  Support for additional EVM networks
- [ ]  Gasless experience for influencers during fund withdrawal: account abstraction or transaction initiation from IC

## Future Vision

The B4B protocol serves as an open reputation layer, providing a permissionless infrastructure for decentralized social networks and the creator economy. This core protocol will be implemented on ICP, facilitating the storage of on-chain reputation data from various sources: social data, wallet on-chain data, expertise, and user feedback.

AI predictions and reputation algorithms will be based on the AI Hub of Reputation Data. This will enable advertisers to predict the outcomes of publishing ad content with influencers/KOLs globally. Moreover, creators will maintain ownership of their reputation and portfolio across all social platforms.

## License
This project is licensed under the MIT license, see LICENSE.md for details.

## References
- [Internet Computer](https://internetcomputer.org)
- [B4B](https://b4b.app)
- [ic-siwe](https://github.com/kristoferlund/ic-siwe/tree/main)
- [evm-rpc-canister](https://github.com/internet-computer-protocol/evm-rpc-canister)