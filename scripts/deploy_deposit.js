const { ethers } = require("hardhat");

async function main() {
    const [deployer] = await ethers.getSigners();

    console.log("Deploying contracts with the account:", deployer.address);

    const Deposit = await ethers.getContractFactory("USDCDeposit");
    const depositContract = await Deposit.deploy();

    console.log("Contract address:", await depositContract.getAddress());
}
  
main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });