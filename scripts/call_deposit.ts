const { ethers } = require("hardhat");
import { Principal } from "@dfinity/principal";

const CONTRACT_ADDR = "0x613F03675431Ae726cAb1da9a213A5BfABd6fe19";

async function main() {
    const [deployer] = await ethers.getSigners();

    const value = ethers.parseEther("1");

    console.log(`Calling 'deposit' contracts with the account=${deployer.address} and value=${value}`);

    const Deposit = await ethers.getContractFactory("USDCDeposit");

    const depositContract = Deposit.attach(CONTRACT_ADDR);

    const principal = Principal.from("kfew6-hal2v-vluno-gpyon-nmlmv-bvp2p-zk2ke-s62nv-o7ire-lkpwx-uae");
    const hexPrincipal = `0x000000${principal.toHex()}`;
    const tx = await depositContract.deposit(value, hexPrincipal);
    console.log(`Transaction hash ${tx.hash}`);
    const receipt = await tx.wait();

    console.log(receipt);
}
  
main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });