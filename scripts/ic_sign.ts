import { Wallet } from "ethers";
import { SiweProvider } from "../src";
// import { SiweProvider } from "@b4b-world/b4b-icp";


async function main() {
    const wallet = Wallet.createRandom();

    const canisterId = "bkyz2-fmaaa-aaaaa-qaaaq-cai";
    const host = 'http://127.0.0.1:4943';
    const actor = new SiweProvider(canisterId, host);

    console.log(wallet.address);

    const prepareResponse = await actor.prepareLoginOrThrow(wallet.address);

    const signature = await wallet.signMessage(prepareResponse);
    const [loginDetails, sessionIdentity] = await actor.loginOrThrow(wallet.address, signature);

    const [delegationIdentity, delegationChain] = await actor.getDelegationIdentityOrThrow(wallet.address, sessionIdentity, loginDetails);

    console.log(delegationIdentity);
}


main()
    .then(() => {
        console.log("Finished!");
    });