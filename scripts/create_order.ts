import { getCoreActor, CreateOrderInput } from '../src/icp/core';
import { SiweProvider } from '../src/icp/siwe';
import { recoverDelegationOrThrow, encodeVerificationRequest, decodeVerificationRequest } from '../src/icp/siwe/helpers';
import { getContractsForChainOrThrow } from '../src/evm'
import { Wallet, JsonRpcProvider, hexlify } from 'ethers';

import { HttpAgent, PublicKey, requestIdOf } from '@dfinity/agent';
import { Principal } from '@dfinity/principal';
import { Delegation } from '@dfinity/identity';

require('dotenv').config();

const ETH_PRIVATE_KEY = process.env.ETH_PRIVATE_KEY;

const RPC_URL = 'https://polygon-testnet.public.blastapi.io';
const SIWE_CANISTER_ID = 'bd3sg-teaaa-aaaaa-qaaba-cai';
const CANISTER_ID = 'bkyz2-fmaaa-aaaaa-qaaaq-cai';
const HOST = 'http://127.0.0.1:4943';

async function main() {
    const provider = new JsonRpcProvider(RPC_URL);
    const signer = new Wallet(ETH_PRIVATE_KEY!, provider);
    const address = signer.address;

    const contracts = getContractsForChainOrThrow(80001, signer);

    const agent = new HttpAgent({ host: HOST })
    const rootKey = await agent.fetchRootKey();

    const siwe = new SiweProvider(SIWE_CANISTER_ID, HOST);
    
    const nonce = await siwe.prepareLoginOrThrow(address);

    // const decoded = JSON.parse(nonce);
    // console.log(nonce);

    const signature = await signer.signMessage(nonce);

    const [loginDetails, sessionIdentity] = await siwe.loginOrThrow(address, signature);
    const [delegationIdentity, delegationChain] = await siwe.getDelegationIdentityOrThrow(address, sessionIdentity, loginDetails);

    const publicKey = new Uint8Array(delegationIdentity.getPublicKey().toDer());

    const request = {
        signatureBytes: new Uint8Array(delegationChain.delegations[0].signature),
        challenge: new Uint8Array(requestIdOf(delegationChain.delegations[0].delegation)),
        publicKey: new Uint8Array(delegationIdentity.getPublicKey().toDer())
    };
    
    const requestJson = encodeVerificationRequest(request);
    const decodedRequest = decodeVerificationRequest(requestJson);


    const principal = await recoverDelegationOrThrow(requestJson, new Uint8Array(rootKey));
    console.log(principal.toString());


    // const principal = await siwe.getPrincipal(address);
    const hexPrincipal = `0x000000${principal.toHex()}`;
    console.log(hexPrincipal);


    // const tx = await contracts.depositContract.deposit(100, hexPrincipal);
    // const res = await tx.wait();

    // const actor = getCoreActor(CANISTER_ID, HOST, delegationIdentity);

    // const input: CreateOrderInput = {
    //     'influencer_id' : BigInt(1),
    //     'release_timestamp' : BigInt(100),
    //     'chain_id' : BigInt(80001),
    //     'order_type' : 1,
    //     'data_hash' : "0x0",
    //     'amount' : BigInt(100),
    //     'payment_tx_hash' : res!.hash,
    // };
    // const result = await actor.create_order(input);
    // console.log(result);

    // approve
    // withdraw -> certificate
    // 
}

main()
    .then(() => {
        "Finished!"
    });