import { EventListener } from '../src/icp/listener';
import { ICP_HOST, ICP_SIWE_PROVIDER_CANISTER_ID, BACKEND_CANISTER_ID} from '../src/icp';


const CANISTER_ID = 'bkyz2-fmaaa-aaaaa-qaaaq-cai';
const HOST = 'http://127.0.0.1:4943';

async function main() {
    // const listener = new EventListener(CANISTER_ID, HOST);
    const listener = new EventListener(BACKEND_CANISTER_ID, ICP_HOST);

    listener.on((patches) => {
        console.log(patches.patches);
    });

    await listener.start(0);
}

main()
    .then(() => {
        "Finished!"
    });