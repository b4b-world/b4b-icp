const { ethers } = require("hardhat");

async function main() {
    const [deployer] = await ethers.getSigners();

    const Deposit = await ethers.getContractFactory("USDCDeposit");
    const depositContract = Deposit.attach("0x1d4412F2444392DD08b2eAbC0bF3234a2c580555");

    const tx = await depositContract.transferOwnership("0x3dc14bf82c4d0ad932843751f33f95ff91ea2317");
    await tx.wait();
    console.log("New owner:", await depositContract.owner());
}
  
main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });