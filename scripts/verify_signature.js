const { ethers } = require("hardhat");

async function main() {
    const [deployer] = await ethers.getSigners();

    console.log("Verifying signature...");

    const Deposit = await ethers.getContractFactory("USDCDeposit");

    const contractAddr = "0xe7f1725E7734CE288F8367e1Bb143E90bb3F0512";
    const depositContract = Deposit.attach(contractAddr);


    const tx = await depositContract.verifySignature(
        1000,
        "0x37679F25Df42Bd6327eaDDc1F56fEF9E54589655",
        1000,
        1000,
        80001,
        "0x37679F25Df42Bd6327eaDDc1F56fEF9E54589655",
        "0x81536b1a02c334dcb52307cb20cf0452554bad5e79222eb7233d6863d5591795155457fc4de1f82782d3c894a5a2fc73e97faa1f9777cc7492680aeba51d6cea1b"
    );
    console.log(tx.hash, tx.blockNumber);
    await tx.wait();
}
  
main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });