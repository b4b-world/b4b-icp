#!/bin/bash

dfx deploy ic_siwe_provider --network ic --argument $'(
    record {
        domain = "*.b4b.app";
        uri = "http://icp.b4b.app";
        salt = ")<,-<U(jLezy4j>*";
        chain_id = opt 1;
        scheme = opt "http";
        statement = opt "Login to the app";
        sign_in_expires_in = opt 300000000000;
        session_expires_in = opt 604800000000000;
        targets = opt vec {
            "'$(dfx canister id ic_siwe_provider --network ic)'";
            "'$(dfx canister id backend --network ic)'";
        };
    }
)'