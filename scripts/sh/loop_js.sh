#!/bin/bash

FILES=`find $1 -type f -name '*.js'`
for file in $FILES
do
  if [[ $file != *.spec.js ]]; then
    echo $file;
    npx babel --plugins @babel/plugin-transform-modules-commonjs $file > $file.copy;
    rm $file;
    mv $file.copy $file;
  fi
done