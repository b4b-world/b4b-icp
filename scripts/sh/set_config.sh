#!/bin/bash

dfx canister call backend $@ set_config $'(
    record {
        evm_rpc_canister_id = principal "'$(dfx canister id evm_rpc $@)'";
        ic_siwe_canister_id = principal "'$(dfx canister id ic_siwe_provider $@)'";
        logs_topic = "0x1fca11352991bcf69c6a62e3eab11afeee11c8e7d581f170dba643613a7e79dc";
        cycle_cost_of_eth_tx_receipt = 450000000;
        response_size_of_eth_tx_receipt = 10000;
        ecdsa_key_name = "test_key_1";
    }
)'

# dfx_test_key