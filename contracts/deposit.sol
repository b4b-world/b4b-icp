// SPDX-License-Identifier: MIT

pragma solidity ^0.8.18;

import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import {IERC20} from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
// import
// import "hardhat/console.sol";

contract USDCDeposit is Ownable {
    using ECDSA for bytes32;

    mapping(uint256 => bool) public used;

    event ReceivedDeposit(uint256 amount, bytes32 principal);

    constructor() Ownable() {}

    function _verifyOwnerSignature(
        bytes32 hash,
        bytes calldata signature
    ) internal view returns (bool) {
        return hash.recover(signature) == owner();
    }

    function withdrawFunds(
        uint256 amount,
        address to,
        uint256 msgid,
        uint64 expiry,
        bytes calldata signature
    ) public {
        require(block.timestamp < expiry, "Signature expired");
        require(!used[msgid], "MsgId already used");
        // (amount, to, order_id, expiry, chain_id, evm_deposit_address)
        require(
            _verifyOwnerSignature(
                keccak256(
                    abi.encode(
                        amount,
                        to,
                        msgid,
                        expiry,
                        block.chainid,
                        address(this)
                    )
                ),
                signature
            ),
            "Invalid signature"
        );

        // transfer(to, amount);
        used[msgid] = true;
    }

    function deposit(uint256 amount, bytes32 principal) public {
        // transferFrom()
        emit ReceivedDeposit(amount, principal);
    }
}
