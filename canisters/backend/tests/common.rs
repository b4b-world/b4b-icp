use candid::{decode_one, encode_args, encode_one, CandidType, Principal};
use pocket_ic::{PocketIc, WasmResult};
use serde::Deserialize;

use ethers_core::abi::{encode, Address};

use backend::{
    memory::{OrderState, OrderStatus, Config, ChainConfig},
    ReturnError,
    LogView, LogViewResponse
};

pub type ReturnResult<T> = Result<T, ReturnError>;
pub type SetConfigResult = ReturnResult<()>;
// pub type CreateOrder

// #[derive(CandidType)]
// pub struct Config {
//     pub evm_rpc_canister_id: Principal,
//     pub logs_topic: String,
//     pub cycle_cost_of_eth_tx_receipt: u128,
//     pub response_size_of_eth_tx_receipt: u64,
//     pub ecdsa_key_name: String,
// }

// #[derive(CandidType)]
// pub struct ChainConfig {
//     pub evm_deposit_address: String,
//     pub rpc_urls: Vec<String>,
// }

pub const VALID_ADDRESS: &str = "0x5aAeb6053F3E94C9b9A09f33669435E7Ef1BeAed";
pub const VALID_RPC_URL: &str = "https://polygon-mumbai-pokt.nodies.app";
pub const VALID_TX_HASH: &str = "0x81536b1a02c334dcb52307cb20cf0452554bad5e79222eb7233d6863d5591795155457fc4de1f82782d3c894a5a2fc73e97faa1f9777cc7492680aeba51d6cea1b";
pub const VALID_CHAIN_ID: u64 = 80001;
pub const ECDSA_KEY_NAME: &str = "master_ecdsa_public_key_fscpm-uiaaa-aaaaa-aaaap-yai";

pub fn create_canister(ic: &PocketIc) -> (Principal, Vec<u8>) {
    let canister_id = ic.create_canister();
    ic.add_cycles(canister_id, 2_000_000_000_000);

    let wasm_path: std::ffi::OsString =
        std::env::var_os("BACKEND_PATH").expect("Missing backend wasm file");
    let wasm_module = std::fs::read(wasm_path).unwrap();

    (canister_id, wasm_module)
}

pub fn update<T: CandidType + for<'de> Deserialize<'de>>(
    ic: &PocketIc,
    sender: Principal,
    canister: Principal,
    method: &str,
    args: Vec<u8>,
) -> Result<T, String> {
    match ic.update_call(canister, sender, method, args) {
        Ok(WasmResult::Reply(data)) => Ok(decode_one(&data).unwrap()),
        Ok(WasmResult::Reject(error_message)) => Err(error_message.to_string()),
        Err(user_error) => Err(user_error.to_string()),
    }
}

pub fn query<T: CandidType + for<'de> Deserialize<'de>>(
    ic: &PocketIc,
    sender: Principal,
    canister: Principal,
    method: &str,
    args: Vec<u8>,
) -> Result<T, String> {
    match ic.query_call(canister, sender, method, args) {
        Ok(WasmResult::Reply(data)) => Ok(decode_one(&data).unwrap()),
        Ok(WasmResult::Reject(error_message)) => Err(error_message.to_string()),
        Err(user_error) => Err(user_error.to_string()),
    }
}

pub fn init(ic: &PocketIc) -> Principal {
    let (canister_id, wasm_module) = create_canister(ic);
    ic.install_canister(canister_id, wasm_module, vec![], None);

    // Fast forward in time to allow the ic_siwe_provider_canister to be fully installed.
    for _ in 0..5 {
        ic.tick();
    }

    let config = Config {
        evm_rpc_canister_id: Principal::anonymous(),
        ic_siwe_canister_id: Principal::anonymous(),
        logs_topic: "".to_string(),
        cycle_cost_of_eth_tx_receipt: 450_000_000,
        response_size_of_eth_tx_receipt: 10_000,
        ecdsa_key_name: ECDSA_KEY_NAME.to_string(),
    };

    let chain_config = ChainConfig {
        evm_deposit_address: VALID_ADDRESS.to_string(),
        rpc_urls: vec![VALID_RPC_URL.to_string()],
    };

    let arg_config = encode_one(config).unwrap();
    let result: Result<SetConfigResult, _> = update(
        ic,
        Principal::anonymous(),
        canister_id,
        "set_config",
        arg_config,
    );
    assert!(result.is_ok());

    let arg_chain_config = encode_args((VALID_CHAIN_ID, chain_config)).unwrap();
    let result: Result<(), _> = update(
        ic,
        Principal::anonymous(),
        canister_id,
        "set_chain_config",
        arg_chain_config,
    );
    assert!(result.is_ok());

    canister_id
}

pub fn create_custom_order(ic: &PocketIc, canister_id: Principal) -> Result<u64, ReturnError> {
    let order = OrderStatus {
        influencer_id: 1,
        advertiser_principal: Principal::anonymous(),
        amount: 1_000,
        creation_timestamp: 100,
        release_timestamp: 100,
        order_type: 1,
        state: OrderState::ResultApproved,
        chain_id: VALID_CHAIN_ID,
        payment_tx_hash: VALID_TX_HASH.to_string(),
        data_hash: "0x0".to_string()
    };

    let arg = encode_one(order).unwrap();
    let result: Result<ReturnResult<u64>, _> = update(
        ic,
        Principal::anonymous(),
        canister_id,
        "create_mocked_order",
        arg,
    );

    result.unwrap()
}

pub fn get_ethereum_signing_address(ic: &PocketIc, canister_id: Principal) -> Address {
    let args = encode_one(()).unwrap();
    let address: Result<String, ReturnError> = query(
        ic,
        Principal::anonymous(),
        canister_id,
        "ethereum_ecdsa_address",
        args,
    )
    .unwrap();

    address.unwrap().parse::<Address>().unwrap()
}

pub fn view_events_log(ic: &PocketIc, canister_id: Principal) -> LogViewResponse {
    let args = encode_one(LogView {
        from: Some(0),
        to: None
    }).unwrap();
    let response: LogViewResponse = query(
        ic,
        Principal::anonymous(),
        canister_id,
        "view_events_log",
        args,
    )
    .unwrap();

    return response;
}