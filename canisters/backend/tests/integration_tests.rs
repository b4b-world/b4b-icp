mod common;

use std::str::FromStr;

use candid::{decode_one, encode_args, encode_one, CandidType, Principal};
use pocket_ic::PocketIc;
// use ethers_core::k256::ecdsa::VerifyingKey;

use ethers_core::{abi::Address, types::Signature};

use backend::{
    message_hash_from_withdrawal_request, CreateOrderInput, ReturnError, WithdrawalCertificate,
};

use common::{
    create_custom_order, get_ethereum_signing_address, init, update, view_events_log, ReturnResult,
    VALID_ADDRESS, VALID_CHAIN_ID, VALID_TX_HASH,
};

#[test]
fn test_init_with_no_settings() {
    let ic = PocketIc::new();
    let (canister_id, wasm_module) = common::create_canister(&ic);
    let sender = None;
    ic.install_canister(canister_id, wasm_module, encode_one(()).unwrap(), sender);
}

#[test]
fn test_create_order() {
    let ic = PocketIc::new();
    let canister_id = init(&ic);

    let influencer_id: u64 = 1;
    let amount: u64 = 1_000;
    let chain_id = VALID_CHAIN_ID;
    let payment_tx_hash = VALID_TX_HASH.to_string();
    let data_hash = "0x0".to_string();
    let sender = Principal::anonymous();
    let release_timestamp = 100;
    let order_type = 1;

    let args = encode_one(CreateOrderInput {
        influencer_id,
        amount,
        order_type,
        chain_id,
        release_timestamp,
        payment_tx_hash,
        data_hash
    })
    .unwrap();
    let res: Result<ReturnResult<u64>, _> = update(&ic, sender, canister_id, "create_order", args);

    let order_id = res.unwrap();

    let response = view_events_log(&ic, canister_id);
    println!("{:?}", response.to);

    // println!("{:?}", res);
}

#[test]
fn test_withdraw_with_valid_order() {
    let ic = PocketIc::new();
    let canister_id = init(&ic);

    let order_id = create_custom_order(&ic, canister_id).unwrap();

    let sender = Principal::anonymous();
    let args = encode_args((order_id, VALID_ADDRESS)).unwrap();
    let result: Result<ReturnResult<WithdrawalCertificate>, _> =
        update(&ic, sender, canister_id, "withdraw_funds", args);
    let certificate_result = result.unwrap();

    let WithdrawalCertificate(withdrawal_request, signature) = certificate_result.unwrap();

    let signature = Signature::from_str(&signature).unwrap();
    let message_hash = message_hash_from_withdrawal_request(&withdrawal_request).unwrap();
    let actual_address = signature.recover(message_hash).unwrap();

    let expected_address = get_ethereum_signing_address(&ic, canister_id);

    assert_eq!(actual_address, expected_address);
}

// #[test]
// fn test_view_logs() {

// }
