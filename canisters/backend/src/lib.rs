pub mod crypto;
mod declarations;
pub mod memory;
pub mod utils;

use candid::{CandidType, Principal};
use ethers_core::abi::{Address, Token};
use ic_cdk::{query, update};

use crypto::{ethereum_address_from_public_key, EcdsaSignature};
use declarations::evm_rpc::{self, RpcError};
use ethers_core::k256::ecdsa::{RecoveryId, Signature, VerifyingKey};
use ethers_core::utils::keccak256;
use ic_cdk::api::management_canister::ecdsa::{
    ecdsa_public_key, sign_with_ecdsa, EcdsaCurve, EcdsaKeyId, EcdsaPublicKeyArgument,
    EcdsaPublicKeyResponse, SignWithEcdsaArgument, SignWithEcdsaResponse,
};
use ic_stable_structures::Storable;
use memory::{
    decode_event, get_chain_config, get_config, get_ecdsa_keys, record_event, Cbor, ChainConfig,
    Config, EcdsaKeys, Event, OrderState, OrderStatus, CHAIN_MAP, CONFIG, ECDSA_KEYS, EVENTS_LOG,
    INFLUENCER_MAP, ORDER_MAP,
};

#[derive(CandidType, candid::Deserialize, Debug)]
pub enum ReturnError {
    GenericError,
    InputError,
    Unauthorized,
    Expired,
    InterCanisterCallError(String),
    TecdsaSignatureError(String),
    EvmAddressParseError,
    MemoryError,
    EthRpcError(RpcError),
    InconsistentResultError,
    InvalidStateError,
}

// TODO: only owner
#[update]
async fn set_config(config: Config) -> Result<(), ReturnError> {
    let ecdsa_key_name = config.ecdsa_key_name.clone();

    CONFIG
        .with(|config_cell| {
            let mut config_cell = config_cell.borrow_mut();
            config_cell.set(Cbor(Some(config)))
        })
        .map(|_| ())
        .map_err(|_| ReturnError::MemoryError)?;

    let key_id = EcdsaKeyId {
        curve: EcdsaCurve::Secp256k1,
        name: ecdsa_key_name,
    };

    let ecdsa_pk_args = EcdsaPublicKeyArgument {
        canister_id: None,
        derivation_path: vec![],
        key_id: key_id.clone(),
    };

    let EcdsaPublicKeyResponse { public_key, .. } = ecdsa_public_key(ecdsa_pk_args)
        .await
        .map_err(|(_, err_msg)| ReturnError::TecdsaSignatureError(err_msg))?
        .0;

    let ecdsa_keys = EcdsaKeys {
        public_key: public_key.clone(),
        signing_address: utils::to_hex(&ethereum_address_from_public_key(&public_key).unwrap()),
    };

    ECDSA_KEYS
        .with(|keys_cell| {
            let mut keys_cell = keys_cell.borrow_mut();
            keys_cell.set(Cbor(Some(ecdsa_keys)))
        })
        .map(|_| ())
        .map_err(|_| ReturnError::MemoryError)?;

    Ok(())
}

// TODO: only owner
#[update]
async fn set_chain_config(chain_id: u64, config: ChainConfig) -> () {
    CHAIN_MAP.with(|chain_config| {
        let mut chain_config = chain_config.borrow_mut();
        chain_config.insert(chain_id, config)
    });
}

// TODO: only owner
#[update]
async fn register_influencer(principal: Principal) -> Result<u64, ReturnError> {
    let influencer_id = INFLUENCER_MAP.with(|im| {
        let im = im.borrow();
        if im.push(&principal).is_err() {
            return Err(ReturnError::MemoryError);
        }
        Ok(im.len() - 1)
    })?;

    record_event(&Event::InfluencerProfileCreated {
        influencer_id,
        principal,
    });

    Ok(influencer_id)
}

#[derive(Clone, CandidType, serde::Deserialize)]
pub struct CreateOrderInput {
    pub influencer_id: u64,
    pub order_type: u8,
    pub release_timestamp: u64, // seconds since UNIX epoch
    pub amount: u64,
    pub chain_id: u64,
    pub payment_tx_hash: String,
    pub data_hash: String,
}

// create_order - args: influencer, type, price, payment_tx_hash
#[update]
async fn create_order(input: CreateOrderInput) -> Result<u64, ReturnError> {
    // check influencer id exist
    // validate tx hash

    // let config = get_config();

    let caller = ic_cdk::caller();
    // let caller_evm_address: Result<String, (ic_cdk::api::call::RejectionCode, String)> =
    //     ic_cdk::call(config.ic_siwe_canister_id, "get_address", caller).await;

    ic_cdk::println!("{:?}", caller);

    let order = OrderStatus {
        influencer_id: input.influencer_id,
        order_type: input.order_type,
        advertiser_principal: caller,
        amount: input.amount,
        creation_timestamp: ic_cdk::api::time() / 1_000_000_000,
        release_timestamp: input.release_timestamp,
        state: OrderState::OrderCreated,
        chain_id: input.chain_id,
        payment_tx_hash: input.payment_tx_hash,
        data_hash: input.data_hash,
    };

    let order_id = ORDER_MAP.with(|om| {
        let om = om.borrow();
        if om.push(&order).is_err() {
            return Err(ReturnError::MemoryError);
        }
        Ok(om.len() - 1)
    })?;

    record_event(&Event::OrderCreated {
        order_id,
        order_status: order,
    });

    Ok(order_id)
}

#[update]
async fn accept_order(order_id: u64) -> Result<(), ReturnError> {
    // TODO: check that deposit is not used in other order

    let caller = ic_cdk::caller();

    let mut order = get_order(order_id)?;

    let (amount, principal) =
        sync_transaction_receipt(order.payment_tx_hash.clone(), order.chain_id).await?;
    // TODO: check
    // amount == order.amount
    // principal == order.advertiser_principal

    order.state = OrderState::OrderAccepted;

    set_order(order_id, &order);

    record_event(&Event::OrderUpdated {
        order_id,
        order_state: order.state,
    });

    Ok(())
}

// // reject order
#[update]
async fn reject_order(order_id: u64) -> Result<(), ReturnError> {
    let mut order = get_order(order_id)?;

    // check
    // - caller
    // - state transition: prev state OrderCreated

    // let caller = ic_cdk::caller();
    if order.state != OrderState::OrderCreated {
        return Err(ReturnError::InvalidStateError);
    }

    order.state = OrderState::OrderRejected;

    set_order(order_id, &order);
    record_event(&Event::OrderUpdated {
        order_id,
        order_state: order.state,
    });
    Ok(())
}

// complete order
#[update]
async fn complete_order(order_id: u64) -> Result<(), ReturnError> {
    let mut order = get_order(order_id)?;

    // check
    // - caller
    // - state transition: prev state OrderCreated
    if order.state != OrderState::OrderAccepted {
        return Err(ReturnError::InvalidStateError);
    }

    order.state = OrderState::OrderFilled;

    set_order(order_id, &order);
    record_event(&Event::OrderUpdated {
        order_id,
        order_state: order.state,
    });
    Ok(())
}

// ? claim order
// ? claim delayed order
// ? claim auto approved

#[update]
async fn aprove_result(order_id: u64) -> Result<(), ReturnError> {
    let mut order = get_order(order_id)?;

    let caller = ic_cdk::caller();
    if order.advertiser_principal != caller {
        return Err(ReturnError::Unauthorized);
    }

    if order.state != OrderState::OrderFilled {
        return Err(ReturnError::InvalidStateError);
    }

    order.state = OrderState::ResultApproved;

    set_order(order_id, &order);
    record_event(&Event::OrderUpdated {
        order_id,
        order_state: order.state,
    });

    Ok(())
}

#[derive(Clone, CandidType, serde::Deserialize)]
pub struct WithdrawalRequest {
    pub request_id: u64,
    pub amount: u64,
    pub expiry: u64,
    pub target_wallet: String,
    pub chain_id: u64,
    pub evm_contract_address: String,
}

#[derive(Clone, CandidType, serde::Deserialize)]
pub struct WithdrawalCertificate(pub WithdrawalRequest, pub String);

#[update]
async fn withdraw_funds(
    order_id: u64,
    target_evm_wallet: String,
) -> Result<WithdrawalCertificate, ReturnError> {
    let order = get_order(order_id)?;

    let caller = ic_cdk::caller();
    // check order state
    // order.influencer_id

    let config = get_config();
    let chain_config = get_chain_config(order.chain_id).unwrap();

    let expiry = ic_cdk::api::time() / 1_000_000_000 + 600; // 10 mins
    let request = WithdrawalRequest {
        request_id: order_id,
        amount: order.amount,
        expiry,
        target_wallet: target_evm_wallet,
        chain_id: order.chain_id,
        evm_contract_address: chain_config.evm_deposit_address,
    };

    let signature = sign_withdrawal_request(&request, config.ecdsa_key_name).await?;

    Ok(WithdrawalCertificate(request, signature))
}

#[update]
async fn sync_transaction_receipt(
    tx_hash: String,
    chain_id: u64,
) -> Result<(u64, Principal), ReturnError> {
    let config = memory::get_config();
    let rpc = evm_rpc::EvmRpc(config.evm_rpc_canister_id);
    let chain_config = memory::get_chain_config(chain_id).ok_or(ReturnError::MemoryError)?;

    let custom_rpc = evm_rpc::RpcServices::Custom {
        chainId: chain_id,
        services: chain_config
            .rpc_urls
            .iter()
            .map(|url| evm_rpc::RpcApi {
                url: url.clone(),
                headers: None,
            })
            .collect(),
    };

    let rpc_config = evm_rpc::RpcConfig {
        responseSizeEstimate: Some(config.response_size_of_eth_tx_receipt),
    };

    let result = rpc
        .eth_get_transaction_receipt(
            custom_rpc,
            Some(rpc_config),
            tx_hash,
            config.cycle_cost_of_eth_tx_receipt,
        )
        .await
        .map_err(|(_, err_msg)| ReturnError::InterCanisterCallError(err_msg))?
        .0;

    let receipt = match result {
        evm_rpc::MultiGetTransactionReceiptResult::Consistent(receipt_result) => {
            match receipt_result {
                Ok(Some(tx_receipt)) => Ok(tx_receipt),
                Ok(None) => Err(ReturnError::GenericError),
                Err(err) => Err(ReturnError::EthRpcError(err)),
            }
        }
        evm_rpc::MultiGetTransactionReceiptResult::Inconsistent(_) => {
            Err(ReturnError::InconsistentResultError)
        }
    }?;

    // TODO: check addresses equality

    let target_log = receipt
        .logs
        .iter()
        .filter(|log| log.topics[0] == config.logs_topic)
        .next()
        .unwrap();

    ic_cdk::println!("{:?}", target_log);

    // decode log data
    let data = utils::from_hex(&target_log.data).ok_or(ReturnError::GenericError)?;
    let types: &[ethers_core::abi::ParamType] = &[
        ethers_core::abi::ParamType::Uint(256),
        ethers_core::abi::ParamType::FixedBytes(32),
    ];
    let decoded_data: Vec<Token> =
        ethers_core::abi::decode(types, &data).map_err(|_| ReturnError::GenericError)?;

    let (amount, principal) = match (&decoded_data[0], &decoded_data[1]) {
        (&Token::Uint(amount), Token::FixedBytes(bytes)) => {
            Ok((amount, Principal::from_bytes(bytes[3..].into()))) // sizeof(bytes32) - sizeof(Principal)
        }
        _ => Err(ReturnError::GenericError),
    }?;

    // TODO: check overflow U256 -> u64
    let amount_u64: u64 = amount.0[0];

    Ok((amount_u64, principal))
}

pub fn message_hash_from_withdrawal_request(
    withdrawal_request: &WithdrawalRequest,
) -> Result<[u8; 32], ReturnError> {
    let target_wallet = withdrawal_request
        .target_wallet
        .parse::<Address>()
        .map_err(|_| ReturnError::EvmAddressParseError)?;
    let evm_contract_address = withdrawal_request
        .evm_contract_address
        .parse::<Address>()
        .map_err(|_| ReturnError::EvmAddressParseError)?;

    // Generate tECDSA signature
    // payload is (amount, to, order_id, expiry, chain_id, evm_deposit_address), 32 bytes each
    let mut payload_to_sign: [u8; 192] = [0; 192];
    payload_to_sign[24..32].copy_from_slice(&withdrawal_request.amount.to_be_bytes());
    payload_to_sign[44..64].copy_from_slice(target_wallet.as_bytes());
    payload_to_sign[88..96].copy_from_slice(&withdrawal_request.request_id.to_be_bytes());
    payload_to_sign[120..128].copy_from_slice(&withdrawal_request.expiry.to_be_bytes());
    payload_to_sign[152..160].copy_from_slice(&withdrawal_request.chain_id.to_be_bytes());
    payload_to_sign[172..192].copy_from_slice(evm_contract_address.as_bytes());

    Ok(keccak256(payload_to_sign))
}

async fn sign_withdrawal_request(
    withdrawal_request: &WithdrawalRequest,
    ecdsa_key_name: String,
) -> Result<String, ReturnError> {
    let message_hash = message_hash_from_withdrawal_request(withdrawal_request)?;

    let key_id = EcdsaKeyId {
        curve: EcdsaCurve::Secp256k1,
        name: ecdsa_key_name,
    };
    let derivation_path = vec![];
    let ecdsa_sign_args = SignWithEcdsaArgument {
        message_hash: message_hash.to_vec(),
        derivation_path,
        key_id,
    };
    let SignWithEcdsaResponse { signature } = sign_with_ecdsa(ecdsa_sign_args)
        .await
        .map_err(|(_, err_msg)| ReturnError::TecdsaSignatureError(err_msg))?
        .0;

    let EcdsaKeys { public_key, .. } = get_ecdsa_keys();
    let public_key = VerifyingKey::from_sec1_bytes(&public_key)
        .map_err(|err| ReturnError::TecdsaSignatureError(err.to_string()))?;

    let recid = RecoveryId::trial_recovery_from_prehash(
        &public_key,
        &message_hash,
        &Signature::from_slice(signature.as_slice()).unwrap(),
    )
    .map_err(|err| ReturnError::TecdsaSignatureError(err.to_string()))?;

    let v = recid.is_y_odd() as u8 + 27;

    let signature = EcdsaSignature::from_signature_v(&signature, v).to_string();

    // Return tECDSA signature
    Ok(signature)
}

#[query]
async fn ethereum_ecdsa_address() -> Result<String, ReturnError> {
    let config = get_ecdsa_keys();

    Ok(config.signing_address)
}

#[derive(Clone, CandidType, serde::Deserialize)]
pub struct LogView {
    pub from: Option<u64>,
    pub to: Option<u64>,
}

#[derive(Clone, CandidType, serde::Deserialize)]
pub struct LogViewResponse {
    pub from: u64,
    pub to: u64,
    pub logs: Vec<memory::Event>,
}

/// View logs in the given range (not including 'to').
/// If 'from' is missing, 0 is used.
/// If 'to' is missing, current length of all logs is used.
#[query]
pub async fn view_events_log(view: LogView) -> LogViewResponse {
    let log_len = EVENTS_LOG.with(|log| log.borrow().len());
    let from = view.from.unwrap_or_default();
    let to = view.to.unwrap_or(log_len).min(log_len);
    let mut logs = Vec::new();
    EVENTS_LOG.with(|log| {
        let log = log.borrow();
        for i in from..to {
            logs.push(decode_event(&log.get(i).clone().unwrap_or_default()))
        }
    });

    LogViewResponse { from, to, logs }
}

#[query]
pub async fn get_lates_event_idx() -> u64 {
    let log_len = EVENTS_LOG.with(|log| log.borrow().len());

    log_len
}

/// Helpers

pub fn get_order(order_id: u64) -> Result<OrderStatus, ReturnError> {
    ORDER_MAP.with(|om| {
        let om = om.borrow();
        om.get(order_id).ok_or(ReturnError::MemoryError)
    })
}

pub fn set_order(order_id: u64, order: &OrderStatus) -> () {
    ORDER_MAP.with(|om| {
        let om = om.borrow();
        om.set(order_id, order);
    });
}

///

// #[cfg(mock)]
#[update]
fn create_mocked_order(order: OrderStatus) -> Result<u64, ReturnError> {
    ORDER_MAP.with(|om| {
        let om = om.borrow();
        if om.push(&order).is_err() {
            return Err(ReturnError::MemoryError);
        }
        Ok(om.len() - 1)
    })
}

ic_cdk::export_candid!();
