use ic_cdk_bindgen::{Builder, Config};
use std::path::PathBuf;

fn main() {
    let manifest_dir =
        PathBuf::from(std::env::var("CARGO_MANIFEST_DIR").expect("Cannot find manifest dir"));
    let evm_rpc = Config::new("evm_rpc");
    let mut builder = Builder::new();
    builder.add(evm_rpc);
    builder.build(Some(manifest_dir.join("declarations")));
}