use candid::{CandidType, Decode, Deserialize, Encode, Principal};
use ic_stable_structures::memory_manager::{MemoryId, MemoryManager, VirtualMemory};
use ic_stable_structures::storable::Bound;
use ic_stable_structures::DefaultMemoryImpl;
use ic_stable_structures::{
    Log, RestrictedMemory, StableBTreeMap, StableCell, StableVec, Storable,
};
use std::borrow::Cow;
use std::cell::RefCell;

type RM = RestrictedMemory<DefaultMemoryImpl>;
type VM = VirtualMemory<RM>;

//// Configs

#[derive(Clone, CandidType, serde::Serialize, serde::Deserialize)]
pub struct Config {
    pub evm_rpc_canister_id: Principal,
    pub ic_siwe_canister_id: Principal,
    pub logs_topic: String,
    pub cycle_cost_of_eth_tx_receipt: u128,
    pub response_size_of_eth_tx_receipt: u64,
    pub ecdsa_key_name: String,
}

#[derive(Clone, CandidType, serde::Serialize, serde::Deserialize)]
pub struct EcdsaKeys {
    pub public_key: Vec<u8>,
    pub signing_address: String,
}

#[derive(CandidType, Deserialize)]
pub struct ChainConfig {
    pub evm_deposit_address: String,
    pub rpc_urls: Vec<String>,
}

#[derive(Default)]
pub struct Cbor<T>(pub T)
where
    T: serde::Serialize + serde::de::DeserializeOwned;

impl<T> std::ops::Deref for Cbor<T>
where
    T: serde::Serialize + serde::de::DeserializeOwned,
{
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T> Storable for Cbor<T>
where
    T: serde::Serialize + serde::de::DeserializeOwned,
{
    fn to_bytes(&self) -> Cow<[u8]> {
        let mut buf = vec![];
        ciborium::ser::into_writer(&self.0, &mut buf).unwrap();
        Cow::Owned(buf)
    }

    fn from_bytes(bytes: Cow<[u8]>) -> Self {
        Self(ciborium::de::from_reader(bytes.as_ref()).unwrap())
    }

    const BOUND: Bound = Bound::Unbounded;
}

impl Storable for ChainConfig {
    fn to_bytes(&self) -> std::borrow::Cow<[u8]> {
        Cow::Owned(Encode!(self).unwrap())
    }

    fn from_bytes(bytes: std::borrow::Cow<[u8]>) -> Self {
        Decode!(bytes.as_ref(), Self).unwrap()
    }

    const BOUND: Bound = Bound::Bounded {
        max_size: 512,
        is_fixed_size: false,
    };
}


#[derive(
    CandidType, serde::Serialize, serde::Deserialize, Default, Clone, Debug, PartialEq, Eq,
)]
pub enum OrderState {
    #[default]
    OrderCreated,
    OrderAccepted,
    ResultApproved,
    OrderRejected,
    OrderFilled
}

impl Storable for OrderState {
    fn from_bytes(bytes: Cow<[u8]>) -> Self {
        match bytes[0] {
            0 => OrderState::OrderCreated,
            1 => OrderState::OrderAccepted,
            2 => OrderState::ResultApproved,
            3 => OrderState::OrderRejected,
            4 => OrderState::OrderFilled,
            _ => panic!("invalid mint state"),
        }
    }

    fn to_bytes(&self) -> Cow<[u8]> {
        Cow::Owned(vec![match self {
            OrderState::OrderCreated => 0,
            OrderState::OrderAccepted => 1,
            OrderState::ResultApproved => 2,
            OrderState::OrderRejected => 3,
            OrderState::OrderFilled => 4
        }])
    }

    const BOUND: Bound = Bound::Bounded {
        max_size: 1,
        is_fixed_size: true,
    };
}

#[derive(Clone, CandidType, Debug, serde::Serialize, serde::Deserialize)]
pub struct OrderStatus {
    pub influencer_id: u64,
    pub advertiser_principal: Principal,
    pub amount: u64,
    pub creation_timestamp: u64, // seconds since UNIX epoch
    pub release_timestamp: u64,
    pub order_type: u8,
    pub state: OrderState,
    pub chain_id: u64,
    pub payment_tx_hash: String,
    pub data_hash: String
}

impl Storable for OrderStatus {
    fn from_bytes(bytes: Cow<[u8]>) -> Self {
        Decode!(bytes.as_ref(), Self).unwrap()
    }

    fn to_bytes(&self) -> std::borrow::Cow<[u8]> {
        Cow::Owned(Encode!(self).unwrap())
    }

    const BOUND: Bound = Bound::Bounded {
        max_size: 350,
        is_fixed_size: false,
    };
}

/// Events log

#[derive(candid::CandidType, Clone, Debug, serde::Serialize, serde::Deserialize)]
pub enum Event {
    OrderCreated {
        order_id: u64,
        order_status: OrderStatus,
    },
    OrderUpdated {
        order_id: u64,
        order_state: OrderState
    },
    InfluencerProfileCreated {
        influencer_id: u64,
        principal: Principal
    }
}

/// Storage layout

const CONFIG_SIZE: u64 = 512;
const ECDSA_KEYS_PAGE_END: u64 = CONFIG_SIZE + 128;

const CHAIN_MAP_MEM_ID: MemoryId = MemoryId::new(0);
const INFLUENCER_MAP_MEM_ID: MemoryId = MemoryId::new(1);
const ORDER_MAP_MEM_ID: MemoryId = MemoryId::new(2);

const LOG_IDX_ID: MemoryId = MemoryId::new(3);
const LOG_MEM_ID: MemoryId = MemoryId::new(4);

thread_local! {

    static MEMORY_MANAGER: RefCell<MemoryManager<RM>> = RefCell::new(
        MemoryManager::init(RM::new(DefaultMemoryImpl::default(), ECDSA_KEYS_PAGE_END..u64::MAX/65536-1))
    );

    // protocol configs
    pub static CONFIG: RefCell<StableCell<Cbor<Option<Config>>, RM>> =
        RefCell::new(StableCell::init(
            RM::new(DefaultMemoryImpl::default(), 0..CONFIG_SIZE),
            Cbor::default(),
        ).expect("failed to initialize")
    );

    pub static ECDSA_KEYS: RefCell<StableCell<Cbor<Option<EcdsaKeys>>, RM>> =
        RefCell::new(StableCell::init(
            RM::new(DefaultMemoryImpl::default(), CONFIG_SIZE..ECDSA_KEYS_PAGE_END),
            Cbor::default(),
        ).expect("failed to initialize")
    );

    // map chain_id -> chain_config
    pub static CHAIN_MAP: RefCell<StableBTreeMap<u64, ChainConfig, VM>> =
        MEMORY_MANAGER.with(|mm| {
            RefCell::new(StableBTreeMap::init(mm.borrow().get(CHAIN_MAP_MEM_ID)))
    });

    // influencer_id -> influencer's principal
    pub static INFLUENCER_MAP: RefCell<StableVec<Principal, VM>> =
        MEMORY_MANAGER.with(|mm| {
            RefCell::new(
                StableVec::init(mm.borrow().get(INFLUENCER_MAP_MEM_ID)).unwrap()
            )
    });

    // order_id -> order_status
    pub static ORDER_MAP: RefCell<StableVec<OrderStatus, VM>> =
        MEMORY_MANAGER.with(|mm| {
            RefCell::new(
                StableVec::init(mm.borrow().get(ORDER_MAP_MEM_ID)).unwrap()
            )
    });

    // logs
    pub static EVENTS_LOG: RefCell<Log<Vec<u8>, VM, VM>> =
        MEMORY_MANAGER.with(|mm| {
            RefCell::new(Log::init(mm.borrow().get(LOG_IDX_ID), mm.borrow().get(LOG_MEM_ID)).unwrap())
    });
}

pub fn get_config() -> Config {
    CONFIG.with(|config| {
        let config = config.borrow();
        config.get().0.clone().unwrap()
    })
}

pub fn get_ecdsa_keys() -> EcdsaKeys {
    ECDSA_KEYS.with(|keys| {
        let keys = keys.borrow();
        keys.get().0.clone().unwrap()
    })
}

pub fn get_chain_config(chain_id: u64) -> Option<ChainConfig> {
    CHAIN_MAP.with(|config| {
        let config = config.borrow();
        config.get(&chain_id)
    })
}

/// Encodes an event into a byte array.
fn encode_event(event: &Event) -> Vec<u8> {
    let mut buf = Vec::new();
    ciborium::ser::into_writer(event, &mut buf).expect("failed to encode an event");
    buf
}

/// This function panics if the event decoding fails.
pub fn decode_event(buf: &[u8]) -> Event {
    ciborium::de::from_reader(buf).expect("failed to decode an event")
}

/// Records a new event.
pub fn record_event(event: &Event) {
    let bytes = encode_event(event);
    EVENTS_LOG.with(|events| {
        events
            .borrow()
            .append(&bytes)
            .expect("failed to append an entry to the event log")
    });
}
